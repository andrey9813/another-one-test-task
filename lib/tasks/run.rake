require 'test_task.rb'

namespace :test_task do
  desc 'Run test task'
  task :run do
    plate = TestTask::Plate.new(x: 5, y: 5)
    robots, commands = build_trip_data \
      data: data_from_user

    TestTask::Exploration.robots = robots
    TestTask::Exploration.commands = commands
    TestTask::Exploration.plate = plate
    result = TestTask::Exploration.execute
    puts "\nResults:"
    result.each do |robot|
      p "#{robot.x},#{robot.y},#{robot.direction}"
    end
  end

  def data_from_user
    puts "Enter robots data:"
    index = 0
    data = {}
    loop do
      puts "data for robot #{index}"
      puts "enter robot's coordinates and direction"
      robot_data = STDIN.gets.chomp
      puts "enter robot's commands"
      commands = []
      loop do
        command = STDIN.gets.chomp
        commands << command if command != 'REPORT'
        break if command == 'REPORT'
      end
      data[index] = { robot_data: robot_data, commands: commands }
      index += 1
      puts "Add another one? [Y/n]"
      break if STDIN.gets.chomp.casecmp('n').zero?
    end
    data
  end

  def build_trip_data(data:)
    robots, commands = [], []
    data.each do |index, value|
      x, y, direction = *value[:robot_data].split(' ')[1].split(',')
      robots.push TestTask::Robot.new \
        index: index, x: x.to_i, y: y.to_i, direction: direction
      commands << value[:commands]
    end
    return robots, commands
  end
end
