require "test_task/version"

require "test_task/robot.rb"
require "test_task/plate.rb"
require "test_task/exploration.rb"
require "test_task/exploration/validator.rb"
require "test_task/exploration/robot_trip.rb"
require "test_task/exploration/action.rb"
require "test_task/exploration/action/validator.rb"

require 'byebug'

module TestTask
  # Your code goes here...
  class Error < ::StandardError; end
end
