module TestTask
  # Represents plate
  class Plate
    attr_reader :x, :y

    def initialize(x:, y:)
      @x = x
      @y = y
    end

    def on_plate?(x:, y:)
      x <= self.x && y <= self.y && x >= 0 && y >= 0
    end
  end
end
