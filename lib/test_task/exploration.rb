module TestTask
  # Represents exploration process
  # contain current state of robots
  class Exploration
    class << self
      attr_accessor :robots, :commands, :plate
    end

    #
    # Execute
    #
    # @return [robot[]] array of robots in final state
    #
    def self.execute
      validator = Validator.new(plate: plate, robots: robots, commands: commands)
      raise(TestTask::Error, validator.errors) unless validator.valid?
      robots.each_with_index do |robot, index|
        params = { robot: robot, plate: plate, commands: commands[index] }
        robots[index] = RobotTrip.new(params).execute
      end
      robots
    end
  end
end
