module TestTask
  # Represents robot
  class Robot
    attr_accessor :index, :x, :y, :direction

    COMMANDS = %w[MOVE RIGHT LEFT].freeze
    DIRECTIONS = %w[NORTH EAST SOUTH WEST].freeze

    def initialize(index:, x:, y:, direction:)
      @index = index
      @x = x
      @y = y
      @direction = direction
    end

    def move
      case @direction
      when 'NORTH'
        @y += 1
      when 'EAST'
        @x += 1
      when 'SOUTH'
        @y -= 1
      when 'WEST'
        @x -= 1
      end
      self
    end

    def turn_right
      @direction = case @direction
                   when 'NORTH'
                     'EAST'
                   when 'EAST'
                     'SOUTH'
                   when 'SOUTH'
                     'WEST'
                   when 'WEST'
                     'NORTH'
                   end
      self
    end

    def turn_left
      @direction = case @direction
                   when 'NORTH'
                     'WEST'
                   when 'EAST'
                     'NORTH'
                   when 'SOUTH'
                     'EAST'
                   when 'WEST'
                     'SOUTH'
                   end
      self
    end
  end
end
