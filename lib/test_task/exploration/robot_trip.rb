module TestTask
  class Exploration
    # Represents trip of one robot
    class RobotTrip
      attr_accessor :robot, :plate, :commands

      def initialize(robot:, plate:, commands:)
        @robot = robot
        @plate = plate
        @commands = commands
      end

      #
      # Execute
      #
      # @return [Robot] robot in final state
      #
      def execute
        return @robot unless @plate.on_plate?(x: @robot.x, y: @robot.y)
        commands.each do |command|
          action = Exploration::Action.new(robot: robot, command: command, plate: plate)
          @robot = action.execute
        end
        @robot
      end
    end
  end
end
