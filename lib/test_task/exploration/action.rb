module TestTask
  class Exploration
    # Executes some command and modify state of robot
    class Action
      attr_accessor :robot, :command, :plate

      def initialize(robot:, command:, plate:)
        @robot = robot
        @command = command
        @plate = plate
      end

      #
      # Execute
      #
      # @return [Robot] robot with new state
      #
      def execute
        case command
        when 'MOVE'
          validator = Validator.new(robot: robot.dup.move, plate: plate)
          return robot if out_of_plate?(validator) || collision?(validator)
          robot.move
        when 'RIGHT'
          robot.turn_right
        when 'LEFT'
          robot.turn_left
        end
        robot
      end

      private

        def out_of_plate?(validator)
          message = 'Robot goes out of the plate'
          !validator.valid? && validator.errors.include?(message)
        end

        def collision?(validator)
          message = 'Robots have collision while running'
          !validator.valid? && validator.errors.include?(message)
        end
    end
  end
end
