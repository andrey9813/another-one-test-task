module TestTask
  class Exploration
    # Validates state of exploration
    class Validator
      attr_reader :errors, :robots, :plate, :commands

      def initialize(robots:, plate:, commands:)
        @errors = []
        @robots = robots
        @plate = plate
        @commands = commands
      end

      def valid?
        validate
        errors.empty?
      end

      private

        def validate
          @errors = []
          @errors << 'Invalid initial state of plate' unless plate_valid?
          @errors << 'Invalid initial state of robot' unless robots_valid?
          @errors << 'Invalid initial state of command' unless commands_valid?
          @errors
        end

        def plate_valid?
          plate.x.is_a?(Integer) && plate.y.is_a?(Integer) &&
            plate.x.positive? && plate.y.positive?
        end

        def robots_valid?
          available_directions = TestTask::Robot::DIRECTIONS
          robot_coords = robots.map do |robot|
            return false unless robot.x.is_a?(Integer) && robot.y.is_a?(Integer)
            return false unless available_directions.include?(robot.direction)
            [robot.x, robot.y]
          end
          robot_coords.size == robot_coords.uniq.size
        end

        def commands_valid?
          commands.each do |robot_commands|
            robot_commands.each do |command|
              return false unless TestTask::Robot::COMMANDS.include?(command)
            end
          end
          true
        end
    end
  end
end
