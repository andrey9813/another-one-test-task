module TestTask
  class Exploration
    class Action
      # Validates state of action
      class Validator
        attr_reader :errors, :robot, :plate

        def initialize(robot:, plate:)
          @errors = []
          @robot = robot
          @plate = plate
        end

        def valid?
          validate
          errors.empty?
        end

        private

          def validate
            @errors = []
            @errors << 'Robot goes out of the plate' unless plate.on_plate?(x: robot.x, y: robot.y)
            @errors << 'Robots have collision while running' if collisions?
            @errors
          end

          def collisions?
            robots = TestTask::Exploration.robots.reject do |robot|
              robot.index == @robot.index
            end
            robots.each do |robot|
              return true if robot.x == @robot.x && robot.y == @robot.y
            end
            false
          end
      end
    end
  end
end
