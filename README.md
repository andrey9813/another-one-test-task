# TestTask

This is the test task.

## Installation

This app require ruby version 2.4.1.

Change directory to folder with this gem and then execute:

    $ bundle install

## Usage

Execute this rake command to run this app:

`bundle exec rake test_task:run`

input format defined in file with task.


## License

The gem is available as open source under the terms of the [MIT License](http://opensource.org/licenses/MIT).
