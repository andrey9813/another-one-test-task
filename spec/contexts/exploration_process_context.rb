RSpec.shared_context :exploration_process_context do
  let(:plate) { TestTask::Plate.new(x: 5, y: 5) }
  let(:first_robot) { TestTask::Robot.new(index: 0, x: 0, y: 0, direction: 'NORTH') }
  let(:second_robot) { TestTask::Robot.new(index: 1, x: 1, y: 1, direction: 'EAST') }
  let(:third_robot) { TestTask::Robot.new(index: 2, x: 4, y: 4, direction: 'SOUTH') }
  let(:first_robot_commands) do
    ['RIGHT', 'LEFT', 'MOVE', 'MOVE', 'RIGHT', 'MOVE', 'MOVE', 'RIGHT', 'LEFT']
  end
  let(:second_robot_commands) do
    ['LEFT', 'RIGHT', 'MOVE', 'MOVE', 'LEFT', 'MOVE', 'MOVE', 'LEFT', 'RIGHT']
  end
  let(:third_robot_commands) { ['MOVE', 'LEFT', 'MOVE', 'LEFT', 'MOVE', 'MOVE'] }
  let(:robots) { [first_robot, second_robot, third_robot] }
  let(:commands) { [first_robot_commands, second_robot_commands, third_robot_commands] }
end
