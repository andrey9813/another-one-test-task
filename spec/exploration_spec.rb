require "spec_helper"
require "contexts/exploration_process_context.rb"

RSpec.describe TestTask::Exploration do
  include_context :exploration_process_context

  before(:each) do
    described_class.plate = plate
    described_class.robots = robots
    described_class.commands = commands
  end

  describe "validation" do
    describe "invalid plate size" do
      let!(:plate) { TestTask::Plate.new(x: -5, y: -5) }
      it "raise exception" do
        expect { described_class.execute }.to raise_exception(TestTask::Error) do |e|
          e.message == ["Invalid init params of plate"]
        end
      end
    end

    describe "invalid command" do
      let!(:first_robot_commands) { ['MOVE', 'Mowe', 'LEFT', 'MOVE', 'MOVE'] }
      it "raise exception" do
        expect { described_class.execute }.to raise_exception(TestTask::Error) do |e|
          e.message == ["Invalid init params of command"]
        end
      end
    end

    describe "robots collision" do
      let!(:second_robot) { TestTask::Robot.new(index: 1, x: 0, y: 0, direction: 'EAST') }
      it "raise exception" do
        expect { described_class.execute }.to raise_exception(TestTask::Error) do |e|
          e.message == ["Invalid init params of robot"]
        end
      end
    end
  end

  describe "exploration process" do
    describe "robot goes out of the plate" do
      let!(:first_robot_commands) do
        ['MOVE', 'MOVE', 'MOVE', 'MOVE', 'MOVE', 'MOVE', 'MOVE', 'MOVE']
      end
      it "skip such commands" do
        described_class.execute
        expect(robots.first.x).to eq 0
        expect(robots.first.y).to eq 5
        expect(robots.first.direction).to eq 'NORTH'
      end
    end

    describe "robot has invalid initial place" do
      let!(:first_robot) { TestTask::Robot.new(index: 1, x: -1, y: -1, direction: 'EAST') }
      it "should stay on initial place" do
        described_class.execute
        expect(robots.first.x).to eq -1
        expect(robots.first.y).to eq -1
        expect(robots.first.direction).to eq 'EAST'
      end
    end

    describe "robots collision in the process of exploration" do
      let(:third_robot_commands) do
        ['RIGHT', 'MOVE', 'LEFT', 'MOVE', 'MOVE', 'MOVE', 'MOVE', 'MOVE', 'MOVE']
      end
      it "skip such commands" do
        described_class.execute
        expect(robots[1].x).to eq 3
        expect(robots[1].y).to eq 3
        expect(robots[1].direction).to eq 'NORTH'
        expect(robots[2].x).to eq 3
        expect(robots[2].y).to eq 4
        expect(robots[2].direction).to eq 'SOUTH'
      end
    end

    describe "for valid input data" do
      subject { described_class.execute }
      it "should provide correct result" do
        expect(subject[0].index).to eq 0
        expect(subject[0].x).to eq 2
        expect(subject[0].y).to eq 2
        expect(subject[0].direction).to eq 'EAST'
        expect(subject[1].index).to eq 1
        expect(subject[1].x).to eq 3
        expect(subject[1].y).to eq 3
        expect(subject[1].direction).to eq 'NORTH'
        expect(subject[2].index).to eq 2
        expect(subject[2].x).to eq 5
        expect(subject[2].y).to eq 5
        expect(subject[2].direction).to eq 'NORTH'
      end
    end
  end
end
